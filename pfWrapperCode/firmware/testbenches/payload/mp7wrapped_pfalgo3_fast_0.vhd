LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY mp7wrapped_pfalgo3_fast_0 IS
  PORT (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    input_0_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_1_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_2_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_3_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_4_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_5_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_6_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_7_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_8_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_9_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_10_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_11_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_12_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_13_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_14_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_15_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_16_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_17_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_18_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_19_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_20_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_21_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_22_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_23_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_24_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_25_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_26_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_27_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_28_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_29_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_30_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_31_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_32_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_33_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_34_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_35_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_36_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_37_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_38_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_39_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_40_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_41_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_42_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_43_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_44_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_45_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_46_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_47_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_48_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_49_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_50_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_51_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_52_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_53_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_54_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_55_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_56_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_57_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_58_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_59_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_60_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_61_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_62_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_63_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_64_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_65_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_66_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_67_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_68_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_69_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_70_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    input_71_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_0_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_1_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_2_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_3_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_4_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_5_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_6_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_7_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_8_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_9_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_10_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_11_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_12_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_13_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_14_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_15_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_16_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_17_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_18_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_19_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_20_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_21_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_22_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_23_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_24_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_25_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_26_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_27_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_28_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_29_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_30_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_31_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_32_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_33_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_34_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_35_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_36_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_37_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_38_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_39_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_40_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_41_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_42_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_43_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_44_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_45_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_46_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_47_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_48_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_49_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_50_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_51_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_52_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_53_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_54_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_55_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_56_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_57_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_58_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_59_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_60_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_61_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_62_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_63_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_64_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_65_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_66_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    output_67_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END mp7wrapped_pfalgo3_fast_0;

ARCHITECTURE mp7wrapped_pfalgo3_fast_0_arch OF mp7wrapped_pfalgo3_fast_0 IS

  COMPONENT mp7wrapped_pfalgo3_fast IS
    PORT (
      ap_clk : IN STD_LOGIC;
      ap_rst : IN STD_LOGIC;
      ap_start : IN STD_LOGIC;
      ap_done : OUT STD_LOGIC;
      ap_idle : OUT STD_LOGIC;
      ap_ready : OUT STD_LOGIC;
      input_0_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_1_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_2_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_3_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_4_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_5_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_6_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_7_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_8_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_9_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_10_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_11_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_12_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_13_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_14_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_15_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_16_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_17_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_18_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_19_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_20_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_21_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_22_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_23_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_24_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_25_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_26_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_27_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_28_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_29_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_30_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_31_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_32_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_33_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_34_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_35_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_36_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_37_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_38_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_39_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_40_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_41_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_42_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_43_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_44_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_45_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_46_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_47_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_48_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_49_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_50_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_51_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_52_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_53_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_54_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_55_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_56_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_57_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_58_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_59_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_60_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_61_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_62_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_63_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_64_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_65_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_66_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_67_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_68_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_69_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_70_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      input_71_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_0_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_1_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_2_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_3_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_4_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_5_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_6_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_7_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_8_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_9_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_10_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_11_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_12_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_13_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_14_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_15_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_16_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_17_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_18_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_19_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_20_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_21_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_22_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_23_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_24_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_25_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_26_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_27_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_28_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_29_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_30_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_31_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_32_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_33_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_34_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_35_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_36_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_37_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_38_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_39_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_40_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_41_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_42_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_43_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_44_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_45_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_46_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_47_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_48_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_49_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_50_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_51_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_52_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_53_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_54_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_55_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_56_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_57_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_58_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_59_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_60_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_61_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_62_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_63_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_64_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_65_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_66_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      output_67_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
  END COMPONENT mp7wrapped_pfalgo3_fast;

BEGIN
  U0 : mp7wrapped_pfalgo3_fast
    PORT MAP (
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_start => ap_start,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      input_0_V => input_0_V,
      input_1_V => input_1_V,
      input_2_V => input_2_V,
      input_3_V => input_3_V,
      input_4_V => input_4_V,
      input_5_V => input_5_V,
      input_6_V => input_6_V,
      input_7_V => input_7_V,
      input_8_V => input_8_V,
      input_9_V => input_9_V,
      input_10_V => input_10_V,
      input_11_V => input_11_V,
      input_12_V => input_12_V,
      input_13_V => input_13_V,
      input_14_V => input_14_V,
      input_15_V => input_15_V,
      input_16_V => input_16_V,
      input_17_V => input_17_V,
      input_18_V => input_18_V,
      input_19_V => input_19_V,
      input_20_V => input_20_V,
      input_21_V => input_21_V,
      input_22_V => input_22_V,
      input_23_V => input_23_V,
      input_24_V => input_24_V,
      input_25_V => input_25_V,
      input_26_V => input_26_V,
      input_27_V => input_27_V,
      input_28_V => input_28_V,
      input_29_V => input_29_V,
      input_30_V => input_30_V,
      input_31_V => input_31_V,
      input_32_V => input_32_V,
      input_33_V => input_33_V,
      input_34_V => input_34_V,
      input_35_V => input_35_V,
      input_36_V => input_36_V,
      input_37_V => input_37_V,
      input_38_V => input_38_V,
      input_39_V => input_39_V,
      input_40_V => input_40_V,
      input_41_V => input_41_V,
      input_42_V => input_42_V,
      input_43_V => input_43_V,
      input_44_V => input_44_V,
      input_45_V => input_45_V,
      input_46_V => input_46_V,
      input_47_V => input_47_V,
      input_48_V => input_48_V,
      input_49_V => input_49_V,
      input_50_V => input_50_V,
      input_51_V => input_51_V,
      input_52_V => input_52_V,
      input_53_V => input_53_V,
      input_54_V => input_54_V,
      input_55_V => input_55_V,
      input_56_V => input_56_V,
      input_57_V => input_57_V,
      input_58_V => input_58_V,
      input_59_V => input_59_V,
      input_60_V => input_60_V,
      input_61_V => input_61_V,
      input_62_V => input_62_V,
      input_63_V => input_63_V,
      input_64_V => input_64_V,
      input_65_V => input_65_V,
      input_66_V => input_66_V,
      input_67_V => input_67_V,
      input_68_V => input_68_V,
      input_69_V => input_69_V,
      input_70_V => input_70_V,
      input_71_V => input_71_V,
      output_0_V => output_0_V,
      output_1_V => output_1_V,
      output_2_V => output_2_V,
      output_3_V => output_3_V,
      output_4_V => output_4_V,
      output_5_V => output_5_V,
      output_6_V => output_6_V,
      output_7_V => output_7_V,
      output_8_V => output_8_V,
      output_9_V => output_9_V,
      output_10_V => output_10_V,
      output_11_V => output_11_V,
      output_12_V => output_12_V,
      output_13_V => output_13_V,
      output_14_V => output_14_V,
      output_15_V => output_15_V,
      output_16_V => output_16_V,
      output_17_V => output_17_V,
      output_18_V => output_18_V,
      output_19_V => output_19_V,
      output_20_V => output_20_V,
      output_21_V => output_21_V,
      output_22_V => output_22_V,
      output_23_V => output_23_V,
      output_24_V => output_24_V,
      output_25_V => output_25_V,
      output_26_V => output_26_V,
      output_27_V => output_27_V,
      output_28_V => output_28_V,
      output_29_V => output_29_V,
      output_30_V => output_30_V,
      output_31_V => output_31_V,
      output_32_V => output_32_V,
      output_33_V => output_33_V,
      output_34_V => output_34_V,
      output_35_V => output_35_V,
      output_36_V => output_36_V,
      output_37_V => output_37_V,
      output_38_V => output_38_V,
      output_39_V => output_39_V,
      output_40_V => output_40_V,
      output_41_V => output_41_V,
      output_42_V => output_42_V,
      output_43_V => output_43_V,
      output_44_V => output_44_V,
      output_45_V => output_45_V,
      output_46_V => output_46_V,
      output_47_V => output_47_V,
      output_48_V => output_48_V,
      output_49_V => output_49_V,
      output_50_V => output_50_V,
      output_51_V => output_51_V,
      output_52_V => output_52_V,
      output_53_V => output_53_V,
      output_54_V => output_54_V,
      output_55_V => output_55_V,
      output_56_V => output_56_V,
      output_57_V => output_57_V,
      output_58_V => output_58_V,
      output_59_V => output_59_V,
      output_60_V => output_60_V,
      output_61_V => output_61_V,
      output_62_V => output_62_V,
      output_63_V => output_63_V,
      output_64_V => output_64_V,
      output_65_V => output_65_V,
      output_66_V => output_66_V,
      output_67_V => output_67_V
    );
END mp7wrapped_pfalgo3_fast_0_arch;
