library std;
use std.env.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.tb_helpers.all;
use STD.TEXTIO.all;
use ieee.std_logic_textio.all;

use work.mp7_brd_decl.all;
use work.mp7_data_types.all;

use work.pf_constants.all;

entity testbench is
end testbench;

architecture behavior of testbench is

  constant verbose : boolean := true;

  constant div240          : integer   := 12;
  constant div40           : integer   := 2;
  constant half_period_240 : time      := 25000 ps / div240;
  constant half_period_40  : time      := 6*half_period_240;
  signal   clk240          : std_logic := '1';
  signal   clk40           : std_logic := '1';
  signal   clk_payload     : std_logic_vector(2 downto 0);
  signal   rst_payload     : std_logic_vector(2 downto 0);
  signal   rst             : std_logic_vector(N_REGION - 1 downto 0) := (others => '0');

  signal iD : ldata(71 downto 0);
  signal oQ : ldata(71 downto 0);

begin

  uut : entity work.mp7_payload
    port map (
      clk               => clk40, -- ipbus signals
      rst               => rst(0),
      ipb_in.ipb_addr   => (others => '0'),
      ipb_in.ipb_wdata  => (others => '0'),
      ipb_in.ipb_strobe => '0',
      ipb_in.ipb_write  => '0',
      ipb_out           => open,
      clk_payload       => clk_payload,
      rst_payload       => rst_payload,
      clk_p             => clk240, -- data clock
      rst_loc           => rst,
      clken_loc         => (others => '1'),
      ctrs              => (others => ("00000000", '0', "000000000000", "000")), -- missing!
      bc0               => open,
      d                 => iD, -- data in
      q                 => oQ, -- data out
      gpio              => open, -- IO to mezzanine connector
      gpio_en           => open -- IO to mezzanine connector (three-state enables)
      );
  -- Clocks
  clk240         <= not clk240 after half_period_240;
  clk40          <= not clk40  after half_period_40;
  clk_payload(0) <= clk240;
  clk_payload(2) <= clk40;

  tb : process
    file F                      : text open read_mode is "mp7pf_input_testfile.dat";
    file FI                     : text open write_mode is "../results/payload_tb.inputs";
    file FO                     : text open write_mode is "../results/payload_tb.results";
    variable L, LO              : line;
    constant PF_LATENCY         : integer := PF_ALGO_LATENCY+5; -- IP core latency + 5 ticks in (de)mux etc.
    variable frames             : ldata(71 downto 0);
    variable iFrame             : integer := 0;
    variable validFrame         : boolean;
    variable remainingEvents    : integer := PF_LATENCY;
    variable outFrames          : ldata(67 downto 0);

  begin  -- process tb

    rst         <= (others => '1');
    rst_payload <= (others => '1');
    -- wait for 3*half_period_40;
    wait for 12*half_period_240;
    rst         <= (others => '0');
    rst_payload <= (others => '0');
    -- wait for 2*half_period_40;  -- wait until global set/reset completes
    wait for 18*half_period_240;  -- wait until global set/reset completes

    while remainingEvents > 0 loop
      if not endfile(F) then
        ReadInFrames(F, validFrame, frames);
      else
        frames := (others => ("00000000000000000000000000000000", '0', '0', '0'));
        remainingEvents := remainingEvents-1;
      end if;

      -- Filling payload
      iD <= frames;

      wait for 2*half_period_240;

      outFrames := oQ(67 downto 0);

      DumpFrames(outFrames, iFrame, FO);
      DumpFrames(frames, iFrame, FI);

      iFrame := iFrame+1;
    end loop;
    finish(0);
  end process tb;

end;
