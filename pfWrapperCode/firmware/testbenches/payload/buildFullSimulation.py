#!/bin/python

import os
import subprocess

buildFileName = "buildVHLDsim.sh"

def main():
    sourceRootFolder = "../../../../build/mp7pf/src/"
    destinationRootFolder = "../"

    pfIPcoreFolder = "GlobalCorrelator_HLS/"
    pfIPcoreFilesPath = sourceRootFolder + pfIPcoreFolder + "proj3-mp7-fast/solution/sim/vhdl/"
    pfIPcorePath = sourceRootFolder + pfIPcoreFolder

    fileList = [
      sourceRootFolder + "ipbus-firmware/components/ipbus_slaves/firmware/hdl/ipbus_reg_types.vhd",
      sourceRootFolder + "mp7/components/mp7_datapath/firmware/hdl/mp7_data_types.vhd",
      sourceRootFolder + "ipbus-firmware/components/ipbus_core/firmware/hdl/ipbus_package.vhd",
      sourceRootFolder + "ipbus-firmware/components/ipbus_core/firmware/hdl/ipbus_fabric_sel.vhd",
      sourceRootFolder + "ipbus-firmware/components/ipbus_slaves/firmware/hdl/ipbus_reg_v.vhd",
      sourceRootFolder + "mp7/boards/mp7/base_fw/mp7xe_690/firmware/hdl/mp7_brd_decl.vhd",
      sourceRootFolder + "mp7/boards/mp7/base_fw/common/firmware/hdl/mp7_top_decl.vhd",
      "../../hdl/top_decl.vhd",
      sourceRootFolder + "mp7/components/mp7_ttc/firmware/hdl/mp7_ttc_decl.vhd",
      sourceRootFolder + "ipbus-firmware/components/ipbus_slaves/firmware/hdl/ipbus_dpram.vhd",
      pfIPcoreFilesPath,
      "mp7wrapped_pfalgo3_fast_0.vhd",
      "../../hdl/pf_constants.vhd",
      "../../hdl/pf_data_types.vhd",
      "../../hdl/mux_quad.vhd",
      "../../hdl/multiplexer.vhd",
      "../../hdl/demux_quad.vhd",
      "../../hdl/demultiplexer.vhd",
      "../../hdl/pf_ip_wrapper.vhd",
      "../../hdl/mp7_payload.vhd",
      "../tb_helpers.vhd",
      "payload_tb.vhd"
    ]
    
    log("Creating VHDL simulation for PF IP core.")
    log("Copying TCL script to " + pfIPcorePath + ".")
    subprocess.call(["cp", "../hlsSimulationSetup.tcl", pfIPcorePath])
    log("Running Vivado HLS to generate VHDL simulation.")
    cur_env = os.environ.copy()
    p = subprocess.Popen(["vivado_hls", "-f", "hlsSimulationSetup.tcl"], env=cur_env, cwd=pfIPcorePath)
    p.wait()

    log("Writing build file")
    writeBuildFile(fileList)

    log("Building simulation")
    subprocess.call(["chmod", "u+x", buildFileName])
    subprocess.call(["./" + buildFileName])

    log("##############################################################################")
    log("Simulation built successfully. To run, execute './runSim.sh'.")
    log("To rebuild full simulation, re-run this script. To rebuild the VHDL part you")
    log("can execute the faster " + buildFileName + " script.")
    log("##############################################################################")

def log(msg):
    print "#### " + msg

def writeBuildFile(fileList):
    with open(buildFileName, "w") as f:
        f.write("#!/bin/bash\n\n")
        f.write("rm -rf payload\n")
        f.write("vlib payload\n")
        f.write("vmap work payload\n\n")
        for fn in fileList:
            buildStatement = getBuildStatement(fn)
            if buildStatement != "":
                f.write(buildStatement + "\n")
        f.write("\n")
        f.write("vmake work > Makefile")
        f.write("\n")


def getBuildStatement(path):
   if not os.path.isfile(path):
       filesInDir = os.listdir(path)
       buildFileList = []
       for f1 in filesInDir:
           if checkBuildFile(f1):
               buildFileList.append("vcom -check_synthesis " + path + "/" + f1)
       return "\n".join(buildFileList)
   else:
       if checkBuildFile(path):
           return "vcom -check_synthesis " + path 
       else:
           return ""

def checkBuildFile(fn):
    if fn[-3:] == "vhd":
        return True
    else:
        return False

if __name__ == "__main__":
    main()
