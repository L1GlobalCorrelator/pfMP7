
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use STD.TEXTIO.all;
use ieee.std_logic_textio.all;
use work.mp7_data_types.all;

package tb_helpers is


  procedure ReadInFrames (
    file F          :     text;
    variable valid  : out  boolean;
    variable frames : out ldata(71 downto 0));

  procedure DumpFrames (
  variable iInput : in ldata;
  variable iFrame : in integer;
  variable FO     : in text);

end;

package body tb_helpers is

  procedure ReadInFrame (
    variable L       : inout line;
    variable oOutput : out   ldata(71 downto 0)) is
    variable word  : std_logic_vector(31 downto 0);
    variable valid : bit;
    variable id    : string(1 to 5);
    variable iFrm  : string(1 to 5);
    variable colon : string(1 to 2);
    variable debug : line;
  begin  -- ReadInputFrame
    read(L, id); -- id
    read(L, iFrm); -- number
    read(L, colon); -- colon


    for iWord in oOutput'low to oOutput'high loop
      oOutput(iWord).strobe := '1';
      read(L, valid);
      oOutput(iWord).valid := to_stdulogic(valid);
      hread(L, word);
      oOutput(iWord).data  := word;
    end loop;  -- iWord
  end ReadInFrame;

  procedure ReadInFrames (
    file F          :     text;
    variable valid  : out  boolean;
    variable frames : out ldata(71 downto 0)) is
    variable L      : line;
  begin  -- ReadInputFrames
    readline(F, L);

    if L.all'length = 0 then
      frames := (others => ("00000000000000000000000000000000", '0', '0', '0'));
      valid  := false;
    elsif L.all(1 to 5) = "Frame" then
      ReadInFrame(L, frames);
      valid := true;
    else
      frames := (others => ("00000000000000000000000000000000", '0', '0', '0'));
      valid  := false;
    end if;
  end ReadInFrames;

  procedure DumpFrames (
    variable iInput : in ldata;
    variable iFrame : in integer;
    variable FO     : in text) is
    variable L      : line;
  begin  -- DumpFrames
    write(L, string'("FRM"));
    write(L, iFrame);
    write(L, string'("    "));
    for iChan in iInput'low to iInput'high loop
      write(L, iInput(iChan).valid);
      write(L, string'(" "));
      hwrite(L, iInput(iChan).data);
      write(L, string'("    "));
    end loop;  -- iFrame
    writeline(FO, L);
  end DumpFrames;

end tb_helpers;
