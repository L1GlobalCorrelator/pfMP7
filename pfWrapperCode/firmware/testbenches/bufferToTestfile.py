#!/bin/python

import argparse
import subprocess

def main():
    p = argparse.ArgumentParser(description="Get filenames.")
    p.add_argument('bufferfile')
    p.add_argument('outfile')
    args = p.parse_args()

    with open(args.outfile, 'w') as of:
        subprocess.call(['sed', 's/v/ /g', args.bufferfile],
                        stdout=of)

if __name__ == '__main__':
    main()
