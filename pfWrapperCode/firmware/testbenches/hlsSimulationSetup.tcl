open_project proj3-mp7-fast
set_top mp7wrapped_pfalgo3_fast
open_solution "solution"
set_part {xc7vx690tffg1927-2}
create_clock -period 4.16667 -name default
set_clock_uncertainty 1.5
config_interface -trim_dangling_port
cosim_design -setup -compiled_library_dir "/home/scratch/modelsim-106a/modeltech/bin" -rtl vhdl -tool modelsim

exit
