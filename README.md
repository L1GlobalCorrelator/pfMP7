pfMP7
============

VHDL wrapper and glue code to integrate particle flow HLS code into the MP7fw code.

## Requirements

The `ipbb` build environment requires Python 2.7, pip, and virtualenv to run. For the firmware build itself Xilinx Vivado 2016.4 and for the simulation Mentor Modelsim 10.6a are used.

To access the MP7 (also in simulation) the IPbus suite as well as the mp7sw package are required.

## Build instructions

A bitfile can be generated in the following way:

```
./scripts/makeProject.sh
./scripts/buildFirmware.sh
```

It can then be found in the folder `build/mp7pf/proj/mp7pf_build/package`.

*Note:* If the build scripts find the file `buildToolSetup.sh` in the base directory they will source it. It can be used to set the environment variables required for Vivado and Modelsim. A template (`buildToolSetup_template.sh`) is provided.

## Payload simulation

Once the project has been built with `./scripts/makeProject.sh` a simulation of the payload part of the firmware can be set up in the following way:

```
cd pfWrapperCode/firmware/testbenches/payload
./buildFullSimulation.py
```

Currently a file named `mp7pf_input_testfile.dat` needs to be provided in the same directory. This essentially follows the formatting rules of an `mp7butler` buffer file, however the valid bit is separated by a space from the 32 bit data word, instead of `v`, e.g.:

```
Frame 0000 : 1 00030008 1 0000cf4e 1 00030007 1 0002cf2c 1 00040011 1 00030975 1 00040009 1 000145c8 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 000a000f 1 00050527 1 0008000b 1 0004dd4f 1 0011002b 1 0002fb86 1 00000033 1 00014bae 1 00000008 1 00034ddb 1 0000001d 1 00010dd9 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 000c000c 1 007b7400 1 000d000d 1 02bbf330 1 000b000b 1 0e3b95b9 1 000c000c 1 0f44efb5 1 00080008 1 0fa6179d 1 00090009 1 0077c476 1 000b000b 1 0f545e89 1 000a000a 1 0f6d7bfc 1 00090009 1 0098d474 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000 1 00000000
```

The simulation can then be run with

```
./runSim.sh
```

In case the HLS code is modified the full simulation needs to be rebuilt using `./buildFullSimulation.py`, however if the VHDL code is modified the faster script `buildVHLDsim.sh` can be used.

## Full board simulation

The MP7 together with the included algorithm can be simulated by Modelsim. This simulation includes the ethernet core and consequently the card can be accessed via a virtual interface over IPbus.

If the project has been set up using `./scripts/makeProject.sh` as described in the above section, the simulation can be run using `./scripts/runFullSimulation.sh`. This command however requires sudo privileges in order to set up the virtual interface.

After starting the simulation the command `ping 192.168.201.1` should show replies from the MP7.

## Access via IPbus

The card can accessed as described in https://twiki.cern.ch/twiki/bin/view/CMS/MP7SoftwareNews

### Quick start

Once mp7sw has been installed the environment can be sourced with `source mp7/tests/env.sh` from inside the mp7sw directory.
The connections file needs to be modified in order to make the software aware of the simulated MP7. To do so add the following to the "connections" section of `mp7/tests/etc/mp7/connections-test.xml`:

```
<connection id="MP7PF_SIM" uri="ipbusudp-2.0://192.168.201.2:50001" address_table="file://${MP7_TESTS}/etc/mp7/addrtab/sim_v2_2_1/mp7_infra.xml" />
```

*Note:* Once IPbus access to the algorithm is needed, we have to provide our own address table file. For now this will however suffice.

The MP7 simulation can then be accessed with

```
mp7butler.py connect --timeout 10000 MP7PF_SIM
```

*Note:* The timeout may need to be increased depending on the speed of the simulating machine.

The board can be reset to use its internal clock with

```
mp7butler.py -v reset --timeout 10000 MP7PF_SIM --clksrc internal
```

*Note:* The commands below will take a significant amount of time to execute on the simulated board. For proper pattern tests it is strongly advised to use the hardware itself.

Patterns can be injected into the input buffers with the command

```
mp7butler.py xbuffers MP7PF_SIM rx PlayOnce --inject file://[path to file]] --timeout 100000
```

and the output buffers are configured for capturing with

```
mp7butler.py xbuffers MP7PF_SIM tx -e 0-67 Capture --timeout 10000
```

Finally, the contents of the buffers can be stored using

```
mp7butler.py capture --tx 0-67 --outputpath=[directory name to store buffer contents in] MP7PF_SIM --timeout 100000
```

In simulation mode it may make sense to just inject patterns and then examine the system using the Modelsim waveform viewer.

## References

### Installation instructions for external packages

* mp7sw: https://twiki.cern.ch/twiki/bin/view/CMS/MP7SoftwareNews#Installation
    * Code can be checked out using `svn co svn+ssh://[username]@svn.cern.ch/reps/cactus/tags/mp7/stable/software/[release]`
* IPbus: https://svnweb.cern.ch/trac/cactus/wiki/uhalQuickTutorial#HowtoInstalltheIPbusSuite

