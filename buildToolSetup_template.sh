# Template for file containing paths and variables for Vivado and Modelsim.
# Replace and uncomment the environment variables below, then remove '_template' from the file name.

## Setup Vivado 2016.4
# export XILINXD_LICENSE_FILE='2112@lxlicen01,2112@lxlicen02,2112@lxlicen03'
# source [path to Vivado installation]/Vivado2016.4/Vivado/2016.4/settings64.sh

## Setup Modelsim
# export MODELSIM_ROOT='[path to Modelsim installation]/modelsim-106a/modeltech/'

## License servers
# export MGLS_LICENSE_FILE='1717@lxlicen01,1717@lxlicen02,1717@lxlicen03;1717@lnxmics1;1717@lxlicen08'
export PATH=$PATH:${MODELSIM_ROOT}/linux_x86_64

