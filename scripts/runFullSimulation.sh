#!/bin/bash
set -e # Exit on error.

if [ -f buildToolSetup.sh ] ; then
    source buildToolSetup.sh
fi

if which vsim &> /dev/null ; then
    echo "Found Mentor Modelsim at" `which vsim`
else
    echo "Couldn't find Mentor Modelsim. Exiting."
    exit 1
fi

BUILD_DIR="build/"
cd ${BUILD_DIR}
source ipbb/env.sh

cd mp7pf/proj/mp7pf_sim/

# Set up virtual interface to talk to MP7
ipbb sim virtualtap

# Run simulation
./vsim work.top -do "run -all"

