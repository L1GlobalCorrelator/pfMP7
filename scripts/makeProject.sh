#!/bin/bash
set -e # Exit on error.

if [ -f buildToolSetup.sh ] ; then
    source buildToolSetup.sh
fi

if which vsim &> /dev/null ; then
    echo "Found Mentor Modelsim at" `which vsim`
else
    echo "Couldn't find Mentor Modelsim. Exiting."
    exit 1
fi

if [ -z ${XILINX_VIVADO:+x} ] ; then
    echo "Xilinx Vivado environment has not been sourced. Exiting."
    exit 1
else
    echo "Found Xilinx Vivado at" ${XILINX_VIVADO}
fi


BUILD_DIR="build/"
BASE_DIR=`pwd`
MP7FW_REPO_SUBDIR="tags/mp7/stable/firmware/mp7fw_v2_2_1"

if [ ! $# -eq 1 ];
then
    echo "WARNING: Path to mp7fw not given, setting to " $MP7FW_REPO_SUBDIR
    echo
else
    MP7FW_REPO_SUBDIR=$1
fi


mkdir $BUILD_DIR
cd $BUILD_DIR

# Anonymous checkout of ipbb
curl -L https://github.com/ipbus/ipbb/archive/v0.2.7.tar.gz | tar xvz
mv ipbb-0.2.7 ipbb

source ipbb/env.sh

ipbb init mp7pf

mkdir mp7pf/src/pfMP7
pushd mp7pf/src/pfMP7
ln -sf ../../../../pfWrapperCode
popd

pushd mp7pf/src
## Get MP7 core firmware
ipbb add git https://gitlab.cern.ch/thea/mp7.git -b standalone
## Get IPbus firmware
ipbb add git https://github.com/ipbus/ipbus-firmware.git
## Get Particle Flow HLS code
ipbb add git https://github.com/p2l1pfp/GlobalCorrelator_HLS -b mp7
pushd GlobalCorrelator_HLS/
vivado_hls -f run_hls_pfalgo3.tcl
popd
popd


pushd mp7pf
ipbb proj create vivado mp7pf_build pfMP7:pfWrapperCode -t top.dep
ipbb proj create sim mp7pf_sim pfMP7:pfWrapperCode -t top_sim.dep
popd

pushd mp7pf/proj/mp7pf_build/
ipbb vivado project reset
# Link in timing checker script.
if [ ! -f "checkTiming.py" ]; then
  ln -s ../../../../scripts/checkTiming.py
fi
# Linking in (temporary) tcl script for PF IP simulation
if [ ! -f "includePF.tcl" ]; then
  ln -s ../../../../scripts/includePF.tcl
fi
vivado -mode batch -source includePF.tcl
popd

pushd mp7pf/proj/mp7pf_sim/
ipbb sim ipcores fli
# Linking in (temporary) tcl script for PF IP simulation
if [ ! -f "includePF.tcl" ]; then
  ln -s ../../../../scripts/includePF.tcl
fi
vivado -mode batch -source includePF.tcl

export IPMODNAME=$(echo -e "source ../../src/GlobalCorrelator_HLS/config_hls_pfalgo3.tcl \n puts \${l1pfTopFunc}" | tclsh)_0
# TODO: Actually need to first generate RTL files and then add those..
vcom top/top.srcs/sources_1/ip/${IPMODNAME}/sim/${IPMODNAME}.vhd
ipbb sim project
